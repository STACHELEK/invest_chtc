
submit:
	condor_submit docker.sub

check:
	condor_q

clean:
	-@rm *.err
	-@rm *.log
	-@rm *.out
	-@rm *_stderror
	