# coding=UTF-8
# hardcoded demo runner script for the ndr model using the sample data
# from natcap.

import time
import sys
import os
import logging
import pandas as pd
from natcap.invest.ndr import ndr

logging.basicConfig(stream=sys.stdout, level=logging.WARN)

def now():
    return int(time.time() * 1000.0)
start_ms = now()
print('[INFO] starting up')

# data_dir = "data/"
# args = {
#     "workspace_dir": "workspace", 
#     "dem_path": data_dir + "DEM_gura.tif", 
#     "lulc_path": data_dir + "land_use_gura.tif", 
#     "runoff_proxy_path": data_dir + "precipitation_gura.tif",
#     "watersheds_path": data_dir + "watershed_gura.shp",
#     "biophysical_table_path": data_dir + "biophysical_table_gura.csv",    
#     "calc_p": True,
#     "calc_n": False,
#     "threshold_flow_accumulation": 1000,
#     "k_param": 2,
#     "subsurface_eff_p": 0, # not used for p model
#     "subsurface_critical_length_p": 0 # not used for p model
#      }
# (pd.DataFrame.from_dict(data=args, orient='index').T.to_csv('args.csv', index = False))
args = pd.read_csv("args.csv").reset_index(drop = True).to_dict("records")[0]

if __name__ == '__main__':    
    print('[INFO] starting execution of the ndr model')
    ndr.execute(args)
    elapsed_time = now() - start_ms
    print('[INFO] finished execution of the ndr model, elapsed time {}ms'.format(elapsed_time))
