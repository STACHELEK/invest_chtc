#!/bin/bash

set -e

# replace env-name on the right hand side of this line with the name of your conda environment
ENVNAME=r-invest
# if you need the environment directory to be named something other than the environment name, change this line
ENVDIR=$ENVNAME
source activate $ENVNAME

python run-ndr.py

tar -czf workspace.tar.gz workspace/
